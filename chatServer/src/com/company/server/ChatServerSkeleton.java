package com.company.server;


import com.company.communication.MessageManager;
import com.company.communication.MethodCallMessage;
import com.company.communication.NetworkAddress;

/**
 * @author Joël Van Eyken
 * @version 1.0 23.09.20 11:14
 */
public class ChatServerSkeleton {

    private final MessageManager messageManager;
    private final ChatServer chatServer;


    public ChatServerSkeleton(MessageManager messageManager) {

        this.messageManager = messageManager;
        System.out.println("my address = " + messageManager.getMyAddress());
        chatServer = new ChatServerImpl();
    }

    private void sendEmptyReply(MethodCallMessage request) {
        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleRegister(MethodCallMessage req) {

        chatServer.register(this.registerHelper(req));
        sendEmptyReply(req);
    }

    private void handleUnregister(MethodCallMessage req) {
        chatServer.unregister(this.registerHelper(req));
        sendEmptyReply(req);
    }

    private Client registerHelper(MethodCallMessage req) {
        String[] socket = req.getParameter("address").split(":");
        NetworkAddress networkAddress = new NetworkAddress(socket[0], Integer.parseInt(socket[1]));
        return new ChatClientStub(networkAddress);
    }

    private void handleSend(MethodCallMessage req) {
        String msg = req.getParameter("message");
        chatServer.send("joel", msg);
        sendEmptyReply(req);
    }



    private void handleRequest(MethodCallMessage request) {
        //System.out.println("ContactsSkeleton:handleRequest(" + request + ")");
        String methodName = request.getMethodName();
        if ("register".equals(methodName)) {
            handleRegister(request);
        } else if ("unregister".equals(methodName)) {
            handleUnregister(request);
        } else if ("send".equals(methodName)) {
            handleSend(request);
        } else {
            System.out.println("ContactsSkeleton: received an unknown request:");
            System.out.println(request);
        }
    }

    private void run()  {
        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            System.out.println(request.getMethodName() + " received in the run loop ");
            handleRequest(request);
        }
    }

    public static void main(String[] args) {

        MessageManager messageManagerServer = new MessageManager();
        ChatServerSkeleton chatServerSkeleton = new ChatServerSkeleton(messageManagerServer);
        chatServerSkeleton.run();
    }
}
