package com.company.server;



/**
 * @author Joël Van Eyken
 * @version 1.0 23.09.20 09:38
 */
public interface ChatServer {

    void register(Client client);

    void unregister(Client client);

    void send(String name, String message);

}
