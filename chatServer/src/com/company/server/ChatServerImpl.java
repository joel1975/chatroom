package com.company.server;


import com.company.communication.NetworkAddress;

import java.util.ArrayList;
import java.util.List;

public class ChatServerImpl implements ChatServer {

    private List<Client> clients;

    public ChatServerImpl() {
        this.clients = new ArrayList<>();
    }

    public void register(Client client) {
        clients.add(client);
        send("server", client.getName() + " has entered the room");

    }

    public void unregister(Client client) {
        clients.remove(client);
        send("server", client.getName() + " has left the room");
    }

    public void send(String name, String message) {
        for(Client client : clients) {
           client.receive(name + ": " +  message);
        }
    }
}
