package com.company.server;


import com.company.communication.MessageManager;
import com.company.communication.MethodCallMessage;
import com.company.communication.NetworkAddress;

import java.util.Objects;

/**
 * @author Joël Van Eyken
 * @version 1.0 23.09.20 12:31
 */
public class ChatClientStub implements Client {


    private final MessageManager messageManager;
    private final NetworkAddress networkAddress;

    public ChatClientStub(NetworkAddress netwerkAddress) {
        this.messageManager = new MessageManager();
        this.networkAddress = netwerkAddress;
    }

    private void checkEmptyReply()  {

        Thread thread = new Thread( () -> {
            String value = "";
            while (!"Ok".equals(value)) {
                MethodCallMessage reply = messageManager.wReceive();
                System.out.println(reply.getMethodName() + " in the check Empty Reply loop, this must be result".toUpperCase());
                if (!"result".equals(reply.getMethodName())) {
                    continue;

                }
                value = reply.getParameter("result");
            }
        });
        thread.setName("CheckEmptyReplyThread");
        thread.start();

    }

    @Override
    public String getName() {

        MethodCallMessage mthCallGetName = new MethodCallMessage(messageManager.getMyAddress(), "getName");
        messageManager.send(mthCallGetName, networkAddress);
        MethodCallMessage reply = messageManager.wReceive();

        if (!"resultName".equals(reply.getMethodName())) {
            return "";
        }
        return reply.getParameter("name");
    }

    public NetworkAddress getNetworkAddress() {
        return this.networkAddress;
    }

    @Override
    public void receive(String msg) {
        MethodCallMessage methodCallMessage = new MethodCallMessage(messageManager.getMyAddress(), "receive");
        methodCallMessage.setParameter("message", msg);
        messageManager.send(methodCallMessage, networkAddress);
        checkEmptyReply();
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ChatClientStub that = (ChatClientStub) o;
        return Objects.equals(networkAddress, that.networkAddress);
    }

    @Override
    public int hashCode() {
        return Objects.hash(networkAddress);
    }
}
