package com.company.clientOne;


import com.company.communication.NetworkAddress;

public class ChatClient implements Client {

    private ChatServer chatServer;   //stub
    private TextReceiver textReceiver;
    private String name;

    // == added ==
    private NetworkAddress networkAddress;

    public ChatClient(ChatServer chatServerStub, String name) {
        this.chatServer = chatServerStub;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void send(String message) {
        chatServer.send(name, message);
    }

    public void receive(String message) {
        if (textReceiver == null) return;
        textReceiver.receive(message);
    }

    public void setTextReceiver(TextReceiver textReceiver) {
        this.textReceiver = textReceiver;
    }

    public void unregister() {
        chatServer.unregister(this);
    }

    public void register() {
        chatServer.register(this);
    }

    // == added ==
    public void setNetworkAddress (NetworkAddress address) {
        this.networkAddress = address;
    }

    // == added ==
    public NetworkAddress getNetworkAddress() {
        return networkAddress;
    }
}
