package com.company.clientOne;

/**
 * @author Joël Van Eyken
 * @version 1.0 23.09.20 12:27
 */
public interface Client {

    void receive(String msg);
    String getName();
}
