package com.company.clientOne;


import com.company.communication.MessageManager;
import com.company.communication.MethodCallMessage;
import com.company.communication.NetworkAddress;

/**
 * @author Joël Van Eyken
 * @version 1.0 23.09.20 15:08
 */
public class ClientChatSkeleton {

    private final MessageManager messageManager;
    private final ChatClient client;

    public ClientChatSkeleton(ChatClient client, MessageManager messageManager ) {

        this.messageManager = messageManager;
        System.out.println("client skeleton: "  + messageManager.getMyAddress());
        this.client = client;
    }

    private void sendEmptyReply(MethodCallMessage request) {

        MethodCallMessage reply = new MethodCallMessage(messageManager.getMyAddress(), "result");
        reply.setParameter("result", "Ok");
        messageManager.send(reply, request.getOriginator());
    }

    private void handleReceive(MethodCallMessage req) {

        String msg = req.getParameter("message");
        this.client.receive(msg);
        sendEmptyReply(req);
    }

    private void handleName(MethodCallMessage req) {

        MethodCallMessage mthCallReturnName = new MethodCallMessage(messageManager.getMyAddress(), "resultName");
        mthCallReturnName.setParameter("name", client.getName());
        messageManager.send(mthCallReturnName, req.getOriginator());
    }

    private void handleRequest(MethodCallMessage req) {
        String methodName = req.getMethodName();
        if ("receive".equals(methodName)) {
            handleReceive(req);
        } else if ("getName".equals(methodName)) {
            handleName(req);
        } else {
            System.out.println("ContactsSkeleton: received an unknown request:");
            System.out.println(req);
        }
    }

    private void run() {

        while (true) {
            MethodCallMessage request = messageManager.wReceive();
            System.out.println(request.getMethodName() + " received in the run loop ".toUpperCase());
            handleRequest(request);
        }
    }


    public static void main(String[] args) {

        if (args.length != 2) {
            System.err.println("Usage: java Client <contactsIP> <contactsPort>");
            System.out.println("main chat clientOne");
            System.exit(1);
        }

        int port = Integer.parseInt(args[1]);
        NetworkAddress chatServerAddress = new NetworkAddress(args[0], port);

        // == Client One ==
        MessageManager messageManagerForStubClientOne = new MessageManager();
        MessageManager messageManagerForSkeletonClientOne = new MessageManager();
        ChatServer chatServerStubOne = new ChatServerStub(chatServerAddress,messageManagerForStubClientOne, messageManagerForSkeletonClientOne.getMyAddress());
        ChatClient chatOne = new ChatClient(chatServerStubOne, "ChatClientOne");
        new ChatFrame(chatOne);
        chatOne.register();
        ClientChatSkeleton skeletonOne = new ClientChatSkeleton(chatOne, messageManagerForSkeletonClientOne );

        Thread clientOneRunThread = new Thread(skeletonOne::run);
        clientOneRunThread.setName("skeletonOneRunThread");
        clientOneRunThread.start();


        // == Client Two ==
        System.out.println("second client");
        MessageManager messageManagerForStubClientTwo = new MessageManager();
        MessageManager messageManagerForSkeletonClientTwo = new MessageManager();
        ChatServer chatServerStubTwo= new ChatServerStub(chatServerAddress,messageManagerForStubClientTwo, messageManagerForSkeletonClientTwo.getMyAddress());
        ChatClient chatTwo = new ChatClient(chatServerStubTwo, "ChatClientTwo");
        new ChatFrame(chatTwo);
        chatTwo.register();
        ClientChatSkeleton skeletonTwo = new ClientChatSkeleton(chatTwo, messageManagerForSkeletonClientTwo );

        Thread clientTwoRunThread = new Thread(skeletonTwo::run);
        clientOneRunThread.setName("skeletonTwoRunThread");
        clientTwoRunThread.start();
    }
}


