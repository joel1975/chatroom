package com.company.clientOne;

public interface TextReceiver {
    void receive(String text);
}
