package com.company.clientOne;


import com.company.communication.MessageManager;
import com.company.communication.MethodCallMessage;
import com.company.communication.NetworkAddress;

/**
 * @author Joël Van Eyken
 * @version 1.0 23.09.20 09:40
 */
public class ChatServerStub implements ChatServer {

    private final NetworkAddress chatServerAddress;
    private final MessageManager messageManager;
    private final NetworkAddress socketRx;

    public ChatServerStub(NetworkAddress chatServerAddress, MessageManager messageManager, NetworkAddress socketRx) {
        this.chatServerAddress = chatServerAddress;
        this.messageManager = messageManager;
        this.socketRx = socketRx;
    }

    private void checkEmptyReply() {

        Thread thread = new Thread( () -> {
            String value = "";
            while (!"Ok".equals(value)) {
                MethodCallMessage reply = messageManager.wReceive();
                System.out.println(reply.getMethodName() + " in the check Empty Reply loop, this must be result".toUpperCase());
                if (!"result".equals(reply.getMethodName())) {
                    continue;

                }
                value = reply.getParameter("result");
            }
        });
        thread.setName("ClientCheckEmptyReplyThread");
        thread.start();
    }

    public void register(Client client) {

        MethodCallMessage msg = new MethodCallMessage(messageManager.getMyAddress(), "register");
        msg.setParameter("address", socketRx.toString());
        messageManager.send(msg, chatServerAddress);
        checkEmptyReply();
    }

    public void unregister(Client client) {

        MethodCallMessage msg = new MethodCallMessage(messageManager.getMyAddress(), "unregister");
        msg.setParameter("address", socketRx.toString());
        messageManager.send(msg, chatServerAddress);
        checkEmptyReply();
    }



    public void send(String name, String message) {

        MethodCallMessage msg = new MethodCallMessage(messageManager.getMyAddress(), "send");
        msg.setParameter("name", name);
        msg.setParameter("message", message);
        messageManager.send(msg, chatServerAddress);
        checkEmptyReply();
    }


}
