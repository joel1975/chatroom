package com.company.communication;

import java.io.IOException;
import java.io.OutputStream;
import java.net.Socket;

/**
 * @author Joël Van Eyken
 * @version 1.0 23.09.20 23:06
 */
public class ClientThread extends Thread {

    Socket destination;
    MethodCallMessage message;

    public ClientThread(Socket destination, MethodCallMessage message) {
        this.destination = destination;
        this.message = message;
    }

    public void run() {
        OutputStream out = null;
        try {
            out = destination.getOutputStream();
            MessageReaderWriter.write(message, out);
            destination.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
