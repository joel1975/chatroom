package com.company.communication;

import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

/**
 * @author Joël Van Eyken
 * @version 1.0 23.09.20 19:32
 */
public class ServerThread extends Thread {

    Socket client;
    MethodCallMessage result = null;

    public ServerThread(Socket socket) {
        this.client = socket;
    }

    public void run () {
     try {
         InputStream in = client.getInputStream();
         result = MessageReaderWriter.read(in);
         client.close();
     } catch (IOException e) {

     }
    }

    public MethodCallMessage getResult() {
        return result;
    }
}
